SMTPTester
==========

A simple command line app to help diagnose & test sending mail via a SMTP server.

Usage
-----

SMTPTester 0.1.0.0
Copyright c Richard Smith 2015

  -a, --address            Required. The server address.

  -p, --port               (Default: 25) The server port.

  -u, --username           (Default: ) The user name to use.

  -p, --password           (Default: ) The password to use.

  --ssl                    (Default: False) Enable SSL.

  --ignore-certificates    (Default: False) Ignore certificate errors.

  --dump-certificates      (Default: False) Dump server certificates to disk.

  -f, --from               (Default: test@test) The address to send from.

  -t, --to                 (Default: test@test) The address to send to.

  -s, --subject            (Default: Test message.) The subject of the message to send.

  -m, --message            (Default: Test message.) The message to send.

  -v, --verbose            (Default: False) Enable verbose output

  --help                   Display this help screen.