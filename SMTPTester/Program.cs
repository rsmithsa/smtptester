﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="Richard Smith">
//     Copyright (c) Richard Smith. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SMTPTester
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Net.Security;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using CommandLine;

    /// <summary>
    /// Main program class for the console application.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Entry point for the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
        private static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args)
                .WithParsed(options =>
                {
                    var message = CreateMessage(options);
                    var client = CreateClient(options);

                    try
                    {
                        Console.WriteLine("Sending...");

                        ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, errors) =>
                        {
                            if (options.DumpCertificates)
                            {
                                try
                                {
                                    Console.WriteLine("Dumping certificate - {0}...", certificate.Subject);
                                    using (var fs = new FileStream(certificate.Subject, FileMode.Create))
                                    {
                                        var cert = certificate.Export(X509ContentType.Cert);
                                        fs.Write(cert, 0, cert.Length);
                                    }

                                    Console.WriteLine("Done.");
                                }
                                catch (Exception)
                                {
                                    Console.WriteLine("Error dumping certificate, please check that you have write access to the working directory.");
                                }
                            }

                            if (options.IgnoreCertificates)
                            {
                                return true;
                            }

                            return errors == SslPolicyErrors.None;
                        };

                        client.Send(message);

                        Console.WriteLine("Success.");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error sending.");
                        WriteException(ex, options);
                    }
                });

#if DEBUG
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey(true);
#endif
        }

        /// <summary>
        /// Creates a <see cref="MailMessage"/> from the <see cref="Options"/>.
        /// </summary>
        /// <param name="options">The options.</param>
        /// <returns>The resulting <see cref="MailMessage"/>.</returns>
        private static MailMessage CreateMessage(Options options)
        {
            return new MailMessage(options.FromAddress, options.ToAddress, options.Subject, options.Message);
        }

        /// <summary>
        /// Creates a <see cref="SmtpClient"/> from the <see cref="Options"/>.
        /// </summary>
        /// <param name="options">The options.</param>
        /// <returns>The resulting <see cref="SmtpClient"/>.</returns>
        private static SmtpClient CreateClient(Options options)
        {
            var client = new SmtpClient(options.Server, options.Port)
                       {
                           EnableSsl = options.UseSsl,
                       };

            if (!string.IsNullOrEmpty(options.UserName) || !string.IsNullOrEmpty(options.Password))
            {
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(options.UserName, options.Password);
            }

            return client;
        }

        /// <summary>
        /// Writes an exception to the console.
        /// </summary>
        /// <param name="ex">The exception.</param>
        /// <param name="options">The options.</param>
        private static void WriteException(Exception ex, Options options)
        {
            Console.WriteLine("Exception:");
            Console.WriteLine(options.Verbose ? ex.ToString() : ex.Message);
            var inner = ex.InnerException;
            while (inner != null)
            {
                Console.WriteLine("Inner Exception:");
                Console.WriteLine(options.Verbose ? inner.ToString() : inner.Message);
                inner = inner.InnerException;
            }
        }
    }
}
