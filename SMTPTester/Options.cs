﻿//-----------------------------------------------------------------------
// <copyright file="Options.cs" company="Richard Smith">
//     Copyright (c) Richard Smith. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace SMTPTester
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using CommandLine;
    using CommandLine.Text;

    /// <summary>
    /// Command line options accepted by the application.
    /// </summary>
    public class Options
    {
        /// <summary>
        /// Gets or sets the server address.
        /// </summary>
        /// <value>The server address.</value>
        [Option('a', "address", Required = true, HelpText = "The server address.")]
        public string Server { get; set; }

        /// <summary>
        /// Gets or sets the server port.
        /// </summary>
        /// <value>The server port.</value>
        [Option("port", Default = 25, HelpText = "The server port.")]
        public int Port { get; set; }

        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        /// <value>The username.</value>
        [Option('u', "username", Default = null, HelpText = "The user name to use.")]
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>The password.</value>
        [Option('p', "password", Default = null, HelpText = "The password to use.")]
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to use SSL.
        /// </summary>
        /// <value><c>true</c> to use SSL; otherwise, <c>false</c>.</value>
        [Option("ssl", Default = false, HelpText = "Enable SSL.")]
        public bool UseSsl { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to ignore certificate errors.
        /// </summary>
        /// <value><c>true</c> to ignore certificate errors; otherwise, <c>false</c>.</value>
        [Option("ignore-certificates", Default = false, HelpText = "Ignore certificate errors.")]
        public bool IgnoreCertificates { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to dump certificates to disk.
        /// </summary>
        /// <value><c>true</c> to dump certificates to disk; otherwise, <c>false</c>.</value>
        [Option("dump-certificates", Default = false, HelpText = "Dump server certificates to disk.")]
        public bool DumpCertificates { get; set; }

        /// <summary>
        /// Gets or sets the address to send from.
        /// </summary>
        /// <value>From address to send from.</value>
        [Option('f', "from", Default = "test@test", HelpText = "The address to send from.")]
        public string FromAddress { get; set; }

        /// <summary>
        /// Gets or sets the address to send to.
        /// </summary>
        /// <value>From address to send to.</value>
        [Option('t', "to", Default = "test@test", HelpText = "The address to send to.")]
        public string ToAddress { get; set; }

        /// <summary>
        /// Gets or sets the subject of the message to send.
        /// </summary>
        /// <value>The subject of the message to send.</value>
        [Option('s', "subject", Default = "Test message.", HelpText = "The subject of the message to send.")]
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets the message to send.
        /// </summary>
        /// <value>The message to send.</value>
        [Option('m', "message", Default = "Test message.", HelpText = "The message to send.")]
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to enable verbose output.
        /// </summary>
        /// <value><c>true</c> to enable verbose output; otherwise, <c>false</c>.</value>
        [Option('v', "verbose", Default = false, HelpText = "Enable verbose output")]
        public bool Verbose { get; set; }
    }
}
